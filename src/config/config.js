module.exports = {
    api: {
        url: "http://pwa-service-desk-api.nsmeiw.pt"
    },
    status: [
        { key: "open", name: "Open"},
        { key: "work_in_progress", name: "In Progress"},
        { key: "closed", name: "Closed"}
    ]
}