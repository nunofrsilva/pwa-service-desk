export const Role = {
    admin: "admin",
    technical: "technical",
    customer: "customer",
    toArray() {
        return [this.admin, this.technical, this.customer]
    }
}