import '@babel/polyfill'
import 'mutationobserver-shim'
import Vue from 'vue'
import './plugins/bootstrap-vue'
import App from './App.vue'
import router from './router'
import store from './store'
import { BootstrapVue,IconsPlugin } from 'bootstrap-vue'
import Chat from 'vue-beautiful-chat'

import './styles/app.scss';

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(Chat)
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
