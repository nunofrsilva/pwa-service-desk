import {authHeader} from "@/helpers/auth-header";
import config from "@/config/config";
import {handleResponse} from "@/helpers/api-response-handler";

export const chatBotService = {
    call
}

/**
 * Call chatbot method
 * @returns {Promise<Response>}
 */
function call(payload) {
    const options = {
        method: 'POST',
        headers: authHeader(),
        body: JSON.stringify(payload)
    }
    options.headers['Content-Type'] = 'application/json';
    return fetch(`${config.api.url}/chat`, options).then(handleResponse)
}
