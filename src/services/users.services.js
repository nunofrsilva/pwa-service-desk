import config from "../config/config";
import { authHeader } from "../helpers/auth-header";
import {handleResponse} from "@/helpers/api-response-handler";

export const usersServices = {
    getAll,
    getAllUsersByRole,
    getById,
    create,
    update,
    deleteById,
    getAllUserNames
}

function getAll() {
    const options = {
        method: 'GET',
        headers: authHeader(),
    }
    return fetch(`${config.api.url}/users`, options).then(handleResponse);
}

function getAllUsersByRole(role) {
    const options = {
        method: 'GET',
        headers: authHeader(),
    }
    return fetch(`${config.api.url}/users?`+ new URLSearchParams({
        role: role
    }), options).then(handleResponse);
}

function getById(id) {
    const options = {
        method: 'GET',
        headers: authHeader(),
    }
    return fetch(`${config.api.url}/users/${id}`, options).then(handleResponse)
}

function create(payload) {
    const options = {
        method: 'POST',
        headers: authHeader(),
        body: JSON.stringify(payload)
    }
    options.headers['Content-Type'] = 'application/json';

    return fetch(`${config.api.url}/users`, options).then(handleResponse)
}

function update(payload) {
    const options = {
        method: 'PUT',
        headers: authHeader(),
        body: JSON.stringify(payload)
    }
    options.headers['Content-Type'] = 'application/json';

    return fetch(`${config.api.url}/users/${payload._id}`, options).then(handleResponse)
}

function deleteById(id) {
    const options = {
        method: 'DELETE',
        headers: authHeader(),
    }
    return fetch(`${config.api.url}/users/${id}`, options).then(handleResponse)
}

function getAllUserNames() {
    const options = {
        method: 'GET',
        headers: authHeader(),
    }
    return fetch(`${config.api.url}/users/all-names`, options).then(handleResponse)
}