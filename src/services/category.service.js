import config from "../config/config";
import { authHeader } from "../helpers/auth-header";
import {handleResponse} from "@/helpers/api-response-handler";

export const categoryService = {
    getAll,
    getById,
    create,
    update,
    deleteById
}

/**
 * Get all categories
 * @returns {Promise<Response>}
 */
function getAll() {
    const options = {
        method: 'GET',
        headers: authHeader(),
    }
    return fetch(`${config.api.url}/categories`, options).then(handleResponse)
}

function getById(id) {
    const options = {
        method: 'GET',
        headers: authHeader(),
    }
    return fetch(`${config.api.url}/categories/${id}`, options).then(handleResponse)
}

function create(payload) {
    const options = {
        method: 'POST',
        headers: authHeader(),
        body: JSON.stringify(payload)
    }
    options.headers['Content-Type'] = 'application/json';

    return fetch(`${config.api.url}/categories`, options).then(handleResponse)
}

function update(payload) {
    const options = {
        method: 'PUT',
        headers: authHeader(),
        body: JSON.stringify(payload)
    }
    options.headers['Content-Type'] = 'application/json';

    return fetch(`${config.api.url}/categories/${payload._id}`, options).then(handleResponse)
}

function deleteById(id) {
    const options = {
        method: 'DELETE',
        headers: authHeader(),
    }
    return fetch(`${config.api.url}/categories/${id}`, options).then(handleResponse)
}
