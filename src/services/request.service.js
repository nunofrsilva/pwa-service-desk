import config from "../config/config";
import { authHeader } from "../helpers/auth-header";
import router from "@/router";
import {authService} from "@/services/auth.service";
import {handleResponse} from "@/helpers/api-response-handler";

export const requestService = {
    getAll,
    getById,
    update,
    create
}
function getAll() {
    const options = {
        method: 'GET',
        headers: authHeader(),
    }
    return fetch(`${config.api.url}/service-request`, options).then(handleResponseList)
}

function getById(id) {
    const options = {
        method: 'GET',
        headers: authHeader(),
    }
    return fetch(`${config.api.url}/service-request/${id}`, options).then(handleResponse)
}

function update(payload) {
    const options = {
        method: 'PUT',
        headers: authHeader(),
        body: JSON.stringify(payload)
    }
    options.headers['Content-Type'] = 'application/json';

    return fetch(`${config.api.url}/service-request/${payload._id}`, options).then(handleResponse)
}

function create(payload) {
    const options = {
        method: 'POST',
        headers: authHeader(),
        body: JSON.stringify(payload)
    }
    options.headers['Content-Type'] = 'application/json';

    return fetch(`${config.api.url}/service-request`, options).then(handleResponse)
}

const handleResponseList = (response) => {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                authService.logout();
                router.push({ name: 'Login' });
            }
            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }
        return data;
    });
};