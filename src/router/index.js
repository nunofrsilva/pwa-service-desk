import Vue from 'vue'
import VueRouter from 'vue-router'
import {Role} from "@/helpers/user-role";
import {authService} from "@/services/auth.service";
import RequestList from "@/views/requests/ListRequests";
import Login from "@/views/Login";
import ShowRequest from "@/views/requests/ShowRequest";
import EditRequest from "@/views/requests/EditRequest";
import CreateRequest from "@/views/requests/CreateRequest";
import ListUsers from "@/views/users/ListUsers";
import ShowUser from "@/views/users/ShowUser";
import EditUser from "@/views/users/EditUser";
import CreateUser from "@/views/users/CreateUser";
import ListCategories from "@/views/categories/ListCategories";
import ShowCategory from "@/views/categories/ShowCategory";
import EditCategory from "@/views/categories/EditCategory";
import CreateCategory from "@/views/categories/CreateCategory";

Vue.use(VueRouter)

/**
 * Routes
 */
const routes = [

  { path: '/login', name: 'Login', component: Login },
  { path: '/service-requests', name: 'ListRequest', component: RequestList,
    meta: { authorize: [Role.admin, Role.technical, Role.customer]}
  },
  { path: '/service-requests/show/:id', name: 'ShowRequest', component: ShowRequest,
    meta: { authorize: [Role.admin, Role.technical, Role.customer]}
  },
  { path: '/service-requests/edit/:id', name: 'EditRequest', component: EditRequest,
    meta: { authorize: [Role.admin, Role.technical]}
  },
  { path: '/service-requests/create', name: 'CreateRequest', component: CreateRequest,
    meta: { authorize: [Role.admin, Role.technical, Role.customer]}
  },
  // USERS
  { path: '/users', name: 'ListUsers', component: ListUsers,
    meta: { authorize: [Role.admin, Role.technical]}
  },
  { path: '/users/show/:id', name: 'ShowUser', component: ShowUser,
    meta: { authorize: [Role.admin, Role.technical]}
  },
  { path: '/users/edit/:id', name: 'EditUser', component: EditUser,
    meta: { authorize: [Role.admin, Role.technical]}
  },
  { path: '/users/create', name: 'CreateUser', component: CreateUser,
    meta: { authorize: [Role.admin, Role.technical]}
  },

  // Categories
  { path: '/categories', name: 'ListCategories', component: ListCategories,
    meta: { authorize: [Role.admin, Role.technical]}
  },
  { path: '/categories/show/:id', name: 'ShowCategory', component: ShowCategory,
    meta: { authorize: [Role.admin, Role.technical]}
  },
  { path: '/categories/edit/:id', name: 'EditCategory', component: EditCategory,
    meta: { authorize: [Role.admin, Role.technical]}
  },
  { path: '/categories/create', name: 'CreateCategory', component: CreateCategory,
    meta: { authorize: [Role.admin, Role.technical]}
  },

  { path: '*', redirect: '/service-requests' }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

/**
 * Router guard, to check if user has login or authorization to access a specific page
 */
router.beforeEach((to, from, next) => {
  const loggedUser = authService.currentUser();
  const { authorize } = to.meta;
  if (authorize) {
    if (!loggedUser) {
      return next('/login');
    } else if (authorize.length && !authorize.includes(loggedUser.role)) {
      return next({ path: '/' });
    }
  }
  next();
});

export default router;
