import {chatBotService} from "@/services/chatbot.service";

export const chatBotModule = {
    namespaced: true,
    state: {message: null},
    getters: {
        message: state => state.message
    },
    actions: {
        async call({dispatch, commit}, text) {
            const payload = {message: text};
            await chatBotService.call(payload).then(
                response => {
                    if (response.intent === "createService") {
                        dispatch('requestsModule/getAll', "User updated successfully!", { root: true });
                    }
                    commit("setMessage", response.body);
                    return response.body;

                }, () => {
                    commit("setMessage", null);
                    return "Ocorreu um erro ao processar o seu pedido. Por favor tente mais tarde."
                }
            )
        },
    },
    mutations: {
        setMessage(state, message) {
            state.message = message;
        }
    }
}