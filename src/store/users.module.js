import {usersServices} from "@/services/users.services";
import router from "@/router";

export const usersModule =  {
    namespaced: true,
    state: {
        users: null,
        selectedUser: null,
        userNames: []
    },
    getters: {
        users: state => state.users,
        selectedUser: state => state.selectedUser,
        getAllUserName: state => state.userNames
    },
    actions: {
        getAll({ commit }) {
            usersServices.getAll().then(
                response => {
                    commit("setUsers", response.body);
                }, error => {
                    console.log(error);
                    commit("setUsers", null)
                }
            );
        },
        getUsersByRole({commit}, role) {
            usersServices.getAllUsersByRole(role).then(
                response => {
                    commit("setUsers", response.body);
                }, error => {
                    console.log(error);
                    commit("setUsers", null);
                }
            );
        },
        getAllUsersNames({commit}) {
            usersServices.getAllUserNames().then(
                response => {
                    commit("setUsersNames", response.body)
                }, error => {
                    console.log(error)
                    commit("setUsersNames", [])
                }
            );
        },
        getById({commit}, id) {
            usersServices.getById(id).then(
                request => {
                    commit("setSelectedUser", request.body)
                }, error => {
                    commit("setSelectedUser", error)
                }
            )
        },
        update({dispatch, commit}, payload) {
            usersServices.update(payload).then(
                response => {
                    commit("setMessage", response.code);
                    dispatch('alert/success', "User updated successfully!", { root: true });
                    router.push({ name: 'ListUsers' })
                }, error => {
                    console.log("error ", error);
                    commit("setMessage", error);
                    dispatch('alert/error', "Error to update user", { root: true });
                }
            )
        },
        create({dispatch, commit}, payload) {
            usersServices.create(payload).then(
                response => {
                    commit("setMessage", response.code);
                    dispatch('alert/success', "User created successfully!", { root: true });
                    router.push({ name: 'ListUsers' })
                }, error => {
                    console.log("error ", error);
                    commit("setMessage", error);
                    dispatch('alert/error', "Error to create user.", { root: true });
                }
            )
        },
        delete({dispatch, commit}, id) {
            usersServices.deleteById(id).then(
                response => {
                    commit("setMessage", response.code);
                    dispatch('alert/success', "User deleted successfully.", { root: true });
                }, error => {
                    commit("setMessage", error);
                    dispatch('alert/error', "Error to delete user..", { root: true });
                }
            )
        }
    },
    mutations: {
        setUsers(state, users) {
            state.users = users;
        },
        setSelectedUser(state, user) {
            state.selectedUser = user;
        },
        setUsersNames(state, users) {
            state.userNames = users
        }
    }
}