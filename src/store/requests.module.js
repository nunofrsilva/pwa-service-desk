import {requestService} from "@/services/request.service";
import router from "@/router";

export const requestsModule = {
    namespaced: true,
    state: {items: null, selectedItem: null, message: null},
    getters: {
        requests: state => state.items,
        selectedRequest: state => state.selectedItem
    },
    actions: {
        getAll({commit}) {
            requestService.getAll()
                .then(
                    requests => {
                        commit("getSuccess", requests.body);
                    }, error => {
                        commit("getFailure", error);
                    }
                )
        },
        getById({commit}, id) {
            requestService.getById(id).then(
                request => {
                    commit("getByIdSuccess", request.body)
                }, error => {
                    commit("getByIdFailure", error)
                }
            )
        },
        update({dispatch, commit}, payload) {
            requestService.update(payload).then(
                response => {
                    commit("setMessage", response.code);
                    dispatch('alert/success', "Service request updated successfully!", { root: true });
                    router.push({ name: 'ListRequest' })
                }, error => {
                    commit("setMessage", error);
                    dispatch('alert/error', "Error to update service request", { root: true });
                }
            )
        },
        create({dispatch, commit}, payload) {
            requestService.create(payload).then(
                response => {
                    commit("setMessage", response.code);
                    dispatch('alert/success', "Service request created successfully!", { root: true });
                    router.push({ name: 'ListRequest' })
                }, error => {
                    commit("setMessage", error);
                    dispatch('alert/error', "Error to create service request", { root: true });
                }
            )
        }
    },
    mutations: {
        getSuccess(state, requests) {
            state.items = requests;
        },
        getFailure(state) {
            state.items = null;
        },
        getByIdSuccess(state, request) {
            state.selectedItem = request;
        },
        getByIdFailure(state) {
            state.selectedItem = null;
        },
        setMessage(state, message) {
            state.message = message;
        }
    }
}