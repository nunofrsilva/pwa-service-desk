import {categoryService} from "@/services/category.service";
import router from "@/router";

export const categoriesModule = {
    namespaced: true,
    state: {categories: null, selectedCategory: null},
    getters: {
        categories: state => state.categories,
        selectedCategory: state => state.selectedCategory
    },
    actions: {
        getAll({commit}) {
            categoryService.getAll().then(
                categories => {
                    commit("setCategories", categories.body);
                }, () => {
                    commit("setCategories", null);
                }
            )
        },
        getById({commit}, id) {
            categoryService.getById(id).then(
                response => {
                    commit("setSelectedCategory", response.body)
                }, () => {
                    commit("setSelectedCategory", null)
                }
            )
        },
        update({dispatch}, payload) {
            categoryService.update(payload).then(
                () => {
                    dispatch('alert/success', "Category updated successfully!", { root: true });
                    router.push({ name: 'ListCategories' })
                }, () => {
                    dispatch('alert/error', "Error to update category", { root: true });
                }
            )
        },
        create({dispatch}, payload) {
            categoryService.create(payload).then(
                () => {
                    dispatch('alert/success', "Category created successfully!", { root: true });
                    router.push({ name: 'ListCategories' })
                }, () => {
                    dispatch('alert/error', "Error to create category.", { root: true });
                }
            )
        },
        delete({dispatch, commit}, id) {
            categoryService.deleteById(id).then(
                () => {
                    dispatch('alert/success', "Category deleted successfully.", { root: true });
                }, error => {
                    commit("setMessage", error);
                    dispatch('alert/error', "Error to delete category.", { root: true });
                }
            )
        }
    },
    mutations: {
        setCategories(state, categories) {
            state.categories = categories;
        },
        setSelectedCategory(state, selectedCategory) {
            state.selectedCategory = selectedCategory;
        }
    }
}