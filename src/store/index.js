import Vue from 'vue'
import Vuex from 'vuex'

import {alert} from "./alert.module";
import {authentication} from "./auth.module";
import {requestsModule} from "@/store/requests.module";
import {categoriesModule} from "@/store/categories.module";
import {usersModule} from "@/store/users.module";
import {chatBotModule} from "@/store/chatbot.module";

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    alert,
    authentication,
    requestsModule,
    categoriesModule,
    usersModule,
    chatBotModule
  }
})
