import { authService } from "@/services/auth.service";
import router from "@/router";
import {Role} from "@/helpers/user-role";
const user = JSON.parse(localStorage.getItem('user'));
const initialState = user ? {status: {loggedIn: true}, user} : {status: {loggingIn: false}, user: null};

export const authentication = {
    namespaced: true,
    state: initialState,
    getters: {
        getUser: state => state.user,
        isAdmin: state => (state.user) ? state.user.role === Role.admin : false ,
        isTechnical: state => (state.user) ? state.user.role === Role.technical: false,
        isCustomer: state => (state.user) ? state.user.role === Role.customer: false,
        isAdminOrTechnical: state => (state.user) ? state.user.role === Role.technical || state.user.role === Role.admin : false
    },
    actions: {
        login({ dispatch, commit }, { email, password }) {
            commit('loginRequest', {email});
            authService.login(email, password)
                .then(
                    user => {
                        commit('loginSuccess', user);
                        router.push({ name: 'ListRequest' })
                    },
                    error => {
                        commit('loginFailure', error);
                        dispatch('alert/error', error, { root: true });
                    }
                );
        },
        logout({ commit }) {
            authService.logout();
            commit('logout');
        }
    },
    mutations: {
        loginRequest(state) {
            state.status = { loggingIn: true };
            state.user = null;
        },
        loginSuccess(state, user) {
            state.status = {loggedIn: true};
            state.user = user;
        },
        loginFailure(state) {
            state.status = {};
            state.user = null;
        },
        logout(state) {
            state.status = {};
            state.user = null;
        }
    }
}
